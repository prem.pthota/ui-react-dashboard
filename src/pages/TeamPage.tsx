import React from "react";
import Chart from "react-apexcharts";
export default class TeamPage extends React.Component<any, any>{
	constructor(props: any) {
		super(props);

		this.state = {


			options: {},
			series: [10,90],
			labels: ['Loss', 'Win'],

		};
	}

	render() {
		return (
			<div className="row">
				<div className="col-lg-4">
					<Chart options={this.state.options} series={this.state.series}  type="pie" width="380" />
				</div>
				<div className="col-lg-4">
					<Chart options={this.state.options} series={this.state.series}  type="pie" width="380" />
				</div>
			</div>
		);
	}
}
