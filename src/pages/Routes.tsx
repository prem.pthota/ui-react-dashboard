import { Route } from "react-router-dom";
import TeamPage from './TeamPage'
export default function Routes() {

	return (
		<Route path="/teams" component={TeamPage} />
	);
}
