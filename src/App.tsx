import './App.css'
import TeamPage from './pages/TeamPage'

export default function App() {
	return (
		<div className="App">
		<TeamPage />
		</div>
	);
}
